
WIFI = {
    'SSID': 'your-ssid',
    'PASSWORD': 'your-password'
}

GRAFANA_TOKEN = 'your-token'

try:
    from local_settings import *
except ImportError:
    pass
