# This is your main script.
from connect import do_connect
from api_client import Client
from machine import Pin
from utime import sleep
from local_settings import GRAFANA_TOKEN, WIFI

# gpio
D4 = Pin(2, Pin.OUT)
D3 = Pin(0, Pin.OUT)


def alert(alert=True):
    if alert:
        D4.off()  # D4 es el led, negado
        D3.on()
    else:
        D4.on()
        D3.off()


def main():
    # conectar wifi
    D4.off()
    do_connect(WIFI['SSID'], WIFI['PASSWORD'])
    sleep(5)
    D4.on()

    api_client = Client(GRAFANA_TOKEN)

    while True:
        # obtener alertas y mostrar alertas
        alert(api_client.is_alerting())
        sleep(15)


main()
