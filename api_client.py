import urequests


class Client(object):
    def __init__(self, token):
        self.token = token
        self.headers = {
            'Authorization': 'Bearer '+token
        }

    def _get_alerts(self):
        try:
            response = urequests.get(
                'https://monitor.ciancoders.com/api/alerts', headers=self.headers)
        except Exception as e:
            return [{'state': e}]
        else:
            return response.json()

    def is_alerting(self):
        alerting = False
        alerts = self._get_alerts()

        # buscar alerta entre todas las alertas
        for a in alerts:
            if a['state'] != 'ok':
                print ('alerting')
                print (a)
                alerting = True
                break

        return alerting
